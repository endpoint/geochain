import generateSummary from '../lib/generateSummary.mjs'
import fs from "fs"

function filterOutNullValues(data) {
  const newObj = {};

  for (const [key, value] of Object.entries(data)) {
    if (value !== null) {
      newObj[key] = value;
    }
  }

  return newObj;
}

function readJsonFile(filePath) {
  try {
    const fileContent = fs.readFileSync(filePath, 'utf-8');
    const jsonArray = fileContent.trim().split('\n').map(jsonString => JSON.parse(jsonString));
    return jsonArray;
  } catch (err) {
    console.error(`Failed to read JSON file: ${err}`);
    return [];
  }
}

const loopOverData = async (data) => {
  for (const rawData of data) {
    const properties = filterOutNullValues(rawData.properties)
    await writeTrainingFile(rawData, properties)
  }
}

const writeTrainingFile = async (rawData, properties) => {
  const summary = await generateSummary(properties);
  const trainingFileContent = `${JSON.stringify(rawData)}\n\n---\n\n${summary}`

  let now = Date.now();
  let data_dir = './training'
  
  fs.writeFile(
    `${data_dir}/${now}.md`,
    trainingFileContent,
    (err) => {
        if (err) {
          console.error(err);
          throw err;
        }
  
        console.log(`${now}.md has been written to file!`);
      }
  );
}

// Read JSON
const data = readJsonFile('./test-data/data_delisted.geojson.json')
loopOverData(data)
