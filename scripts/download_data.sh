#!/bin/sh

DATA_DIR="./raw-data"
TRAIN_DIR="./train"
TEST_DIR="./test-data"

# sudo apt install wget unzip ;

mkdir -p $DATA_DIR ; 

cd $DATA_DIR ;

wget https://data-donnees.ec.gc.ca/data/species/protectrestore/canadian-protected-conserved-areas-database/ProtectedConservedArea.gdb.zip ;

unzip ProtectedConservedArea.gdb.zip ;

echo "deleteing gdb.zip to save space"
rm ProtectedConservedArea.gdb.zip ; 







wget https://data-donnees.ec.gc.ca/data/species/protectrestore/canadian-protected-conserved-areas-database/ProtectedConservedAreaUserManual.pdf ;

wget https://data-donnees.ec.gc.ca/data/species/protectrestore/canadian-protected-conserved-areas-database/ProtectedConservedAreaStatistics.csv ;

cd ..

npm run load_data ;

echo "deleteing gdb to save space" ;
# rm -r ProtectedConservedArea.gdb ; 