// import fgdb from 'fgdb';
// import fs from 'fs';
// import simplify from 'simplify-geojson';

const fgdb = require("fgdb")
const fs = require("fs")
const simplify = require('simplify-geojson')
// var simplified = simplify(geojson, tolerance)
let now = Date.now();

let data_dir = './raw-data'

function writeLayersToFiles(objectOfGeojson) {
  console.log("Got data type: ", typeof objectOfGeojson)
  const layers = Object.keys(objectOfGeojson);
  console.log("Writing: ", layers);
  layers.forEach(layer => {
    // write layer data to file
    fs.writeFile(`${data_dir}/${layer}.json`, JSON.stringify(objectOfGeojson[layer], null, 2), (err) => {
      if (err) {
        console.error(err);
        throw err;
      }

      console.log(`${layer}.json has been written to file!`);
    });
  });
}


fgdb('./raw-data/ProtectedConservedArea.gdb').then((objectOfGeojson) => {

  // console.log("The Data was loded in ", Date.now() - now, "ms");
  // console.log("Layers: ", JSON.stringify(layers, null, 2))
  let layers = Object.keys(objectOfGeojson);

  // The Data was loded in  71505 ms
  // Layers:  [
  //   "ProtectedConservedAreaComment",
  //   "ProtectedConservedAreaDelisted",
  //   "ProtectedConservedArea"
  // ]
  // 



  let areaComment = objectOfGeojson.ProtectedConservedAreaComment;

  let commentKeys = Object.keys(areaComment)
  console.log("THere are this many comments: ", commentKeys.length);
  // commentKeys.forEach(k => {
  //   let comment = areaComment[k];
  //   console.log(comment);
  // })

  let areaDelisted = objectOfGeojson.ProtectedConservedAreaDelisted;

  console.log("Writig Delisted to json #features: ", areaDelisted.features.length);


  let areaListed = objectOfGeojson.ProtectedConservedArea;

  console.log("areaListed has this many features", areaListed.features.length)

  let props = Object.keys(areaListed.features[0].properties);
  let propsD = Object.keys(areaDelisted.features[0].properties);


  props.forEach(p => {
    if (!propsD.includes(p)) {
      console.warn("Property found in props but not delisted:", p)
    }
  })
  propsD.forEach(p => {
    if (!props.includes(p)) {
      console.warn("Property found in props delisted but not listed:", p)
    }
  })

  console.log("Props: ", props);
  console.log("sample coment: ", JSON.stringify(areaComment[0], null, 2))
  console.log("sample delisted: ", JSON.stringify(areaDelisted.features[0].properties, null, 2))

  console.log("sample: ", JSON.stringify(areaListed.features[0], null, 2))

  // let sample = areaListed.features.find(f => JSON.stringify(f).includes("https"));

  // console.log("sample http: ", JSON.stringify(sample.properties))
  // let area_sample = areaListed.features.slice(0, 100)


  let featureCollection = areaListed;
  let n = 10;
  let sample = {
    type: "FeatureCollection",
    features: []
  }
  let i = 0;
  while (sample.features.length < n && i < featureCollection.features.length) {
    let f = simplify(featureCollection.features[i])

    if (JSON.stringify(f.properties).includes("https")) {
      console.log("found a link");
      sample.features.push(f)
    }


    // sample.features.push(f);

    i++
  }



  var myOutput = {
    "sample_data": sample
  }

  console.log("sample_data: ", JSON.stringify(myOutput, null, 2));



  writeLayersToFiles(myOutput);

  // https://docs.berri.ai/api-reference/endpoint/finetuning

  //   "2331"
  // ]
  // layer keys:  ProtectedConservedAreaDelisted :  [
  //   "type",
  //   "features",
  //   "bbox"
  // ]
  // layer keys:  ProtectedConservedArea :  [
  //   "type",
  //   "features",
  //   "bbox"
  // ]
  // console.log("layer keys: ", l, ": ", JSON.stringify(layerKeys, null, 2))


  // })
  //do something
}, function(error) {
  console.error("Failed to load data file", error);
  //do something else
});
