const fs = require('fs');
const { CharacterTextSplitter } = require('langchain/text_splitter');
const { HNSWLib } = require('langchain/vectorstores');
const { OpenAIEmbeddings } = require('langchain/embeddings');
const glob = require("glob");
const util = require("util");
const globPromise = util.promisify(glob);

async function getFiles() {
  try {
    const files = await globPromise("training/**/*.md");
    console.log("Matched files:", files);
    return files;
  } catch (err) {
    console.error("Error:", err);
    throw err;
  }
}

const main = async () => {
  const data = [];
  const files = await getFiles()

for (const file of files) {
  data.push(fs.readFileSync(file, 'utf-8'));
}
  console.log("----------> ", data)
//Essentially, this code reads the contents of all markdown files in a directory and its subdirectories and stores them in an array for later processing.

console.log(`Added ${files.length} files to data.  Splitting text into chunks...`);

const textSplitter = new CharacterTextSplitter({
  chunkSize: 2000,
  separator: "\n\n"
});

let docs = [];
for (const d of data) {
  console.log("D ------>", d)
  const docOutput = textSplitter.splitText(d);
  docs = [...docs, docOutput];
}

console.log("Initializing Store...");

const getStore = async () => {
  const store = await HNSWLib.fromTexts(
    docs,
    docs.map((_, i) => ({ id: i })), // assign an id as the array index
    new OpenAIEmbeddings({
      openAIApiKey: process.env.OPENAI_API_KEY
    })
  )
  return store
}

const store = await getStore()


// console.clear();
console.log("Saving Vectorstore");

store.save("vectorStore")

// console.clear();
console.log("VectorStore saved");
}

main()