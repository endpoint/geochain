// import glob from 'glob';
// import fs from 'fs'
// import { CharacterTextSplitter } from "langchain/text_splitter";
// import { HNSWLib } from "langchain/vectorstores";
// import { OpenAIEmbeddings } from 'langchain/embeddings';
// import { sample_data } from '../raw-data/sample_data.json';
// import { ProtectedConservedAreaComment } from '../raw-data/ProtectedConservedAreaComment.json';

const glob = require('glob');
const fs = require('fs');
const { CharacterTextSplitter } = require('langchain/text_splitter');
const { HNSWLib } = require('langchain/vectorstores');
const { OpenAIEmbeddings } = require('langchain/embeddings');
const { Document } = require('langchain/document');
const { InMemoryDocstore } = require('langchain/docstore');

const sample_data = require('../raw-data/sample_data.json');
const ProtectedConservedAreaComment = require('../raw-data/ProtectedConservedAreaComment.json');

// Steps: 
// 1. Load the sample data

// outputs:
// features => meta, page
// 

// export interface HNSWLibArgs {
//   space: SpaceName;
//   numDimensions?: number;
// }
// export declare class HNSWLib extends SaveableVectorStore {
//     index?: HierarchicalNSWT;
//     docstore: InMemoryDocstore;
//     args: HNSWLibArgs;
//     constructor(args: HNSWLibArgs, embeddings: Embeddings, docstore: InMemoryDocstore, index?: HierarchicalNSWT);
//     addDocuments(documents: Document[]): Promise<void>;
//     addVectors(vectors: number[][], documents: Document[]): Promise<void>;
//     similaritySearchVectorWithScore(query: number[], k: number): Promise<[Document, number][]>;
//     save(directory: string): Promise<void>;
//     static load(directory: string, embeddings: Embeddings): Promise<HNSWLib>;
//     static fromTexts(texts: string[], metadatas: object[], embeddings: Embeddings, docstore?: InMemoryDocstore): Promise<HNSWLib>;
//     static fromDocuments(docs: Document[], embeddings: Embeddings, docstore?: InMemoryDocstore): Promise<HNSWLib>;
// }


let embeding =  new OpenAIEmbeddings({
      openAIApiKey: process.env.OPENAI_API_KEY
    })

let store = new HNSWLib({}, embeding, new InMemoryDocstore())
let docs = [];

class DataModel {

  constructor(listed, delisted, comments) {
    this.listed = listed;
    this.delisted = delisted;
    this.comments = comments;

    this.store = new HNSWLib();

    let docs = featuresToDocs(sample_data);
    this.store.addDocuments(docs)

    this.store.addVectorStore(this.delisted);
    this.store.addVectorStore(this.comments);

    //     /**
    //  * Advanced: Add more documents to an existing VectorStore,
    //  * when you already have their embeddings
    //  */
    // addVectors(vectors: number[][], documents: Document[]): Promise<void>;


  }

  getEmbeding(docId) {
    return this.store.getVectorStore(docId);

  }
  addEmbeding(doc) {

  }
}
function getCommentByZoneId(zone_id) {
  let c = ProtectedConservedAreaComment.find(c => c.ZONE_ID == zone_id)
  return c.COMMENT_E;
}

const data = [];

// console.log(JSON.stringify(sample_data));

let sDocs = featuresToDocs(sample_data);



// const files = await new Promise((resolve, reject) =>
//   glob("training/**/*.md", (err, files) => err ? reject(err) : resolve(files))
// );

// for (const file of files) {
//   data.push(fs.readFileSync(file, 'utf-8'));
// }
//Essentially, this code reads the contents of all markdown files in a directory and its subdirectories and stores them in an array for later processing.

// console.log(`Added ${files.length} files to data.  Splitting text into chunks...`);

const textSplitter = new CharacterTextSplitter({
  chunkSize: 2000,
  separator: "\n"
});

let docs = [];
// for (const d of data) {
//   const docOutput = textSplitter.splitText(d);
//   docs = [...docs, ...docOutput];
// }



function featuresToDocs(featureCollection) {
  if (featureCollection.type !== "FeatureCollection") {
    console.error("Please pass in a valid geojson colection");
    return [];
  }

  return featureCollection.features.map(f => {

    let out = f.properties;
    out.COMMENT_E = getCommentByZoneId(out.ZONE_ID);
    console.log(out.NAME_E, ": ", out.COMMENT_E);
    return JSON.stringify(out)

  })

}
// sample_data.features.forEach(f => {
//   docs.push(f.properties)
//   // todo generate sumary, questions and download links / resorces
//   //also join with comments
//   // we could maybe output a md file here.

// })



function featureToDoc(layer) {
  let out = layer.properties;
  out.COMMENT_E = getCommentByZoneId(out.ZONE_ID);

  let doc = new Document({
    //todo this is one place we could improve the acuracy by fetching more context to save alongside this embeding.
    pageContent: JSON.stringify(out, null, 2),
    metadata: {
      type: "Feature",
      geometry: layer.geometry,
      properties: {
        name: out.NAME_E,
        type: out.TYPE_E,
        link: out.MPLAN_REF,
        desc: out.COMMENT_E,
      }
    }
  })
  return Doc

}

function featureCollectionToDocs(featureCollection) {
  if (featureCollection.type !== "FeatureCollection") {
    console.error("Please pass in a valid geojson colection");
    return [];
  }
  return featureCollection.features.map(f => {
    return featureToDoc(f)
  })
}
async function createVectorStore(docs) {


  console.log("Initializing Store...");


  const store = await HNSWLib.fromTexts(
    docs,
    docs.map((_, i) => ({ id: i })), // assign an id as the array index
    new OpenAIEmbeddings({
      openAIApiKey: process.env.OPENAI_API_KEY
    })
  )

  // console.clear();
  console.log("Saving Vectorstore");

  store.save("vectorStore")

  // console.clear();
  console.log("VectorStore saved");
  return store;
}