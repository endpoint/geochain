
import { useState, useRef, useEffect } from 'react'

import Select from 'react-select';
import sampleData from "../raw-data/sample_data.json"

const styles = {
  fontSize: 14,
  color: 'blue',
  background: "hotpink"
}
// const colourStyles = {
//   control: styles => ({ ...styles, backgroundColor: 'white' }),
//   option: (styles, { data, isDisabled, isFocused, isSelected }) => {
//     const color = chroma(data.color);
//     return {
//       ...styles,
//       backgroundColor: isDisabled ? 'red' : blue,
//       color: '#00FF00',
//       cursor: isDisabled ? 'not-allowed' : 'default',

//     };
//   },

// };
const MySelect = ({ onFeatureSelect, selectOption }) => {
  const options = sampleData.features.map((f, i) => {
    return {
      value: i,
      label: f.properties.NAME_E
    }
  })

  const [isSselected, setSelected] = useState(null);


  const handleChange = function(selected) {


    const i = parseInt(selected)
    console.log("Handleing selecgt feature: ", selected);
    //call the callback for when somthing is selected
    onFeatureSelect(selected.value, selected.label);

    setSelected(selected);
  }

  const customStyles = {
    option: (defaultStyles, state) => ({
      ...defaultStyles,
      color: state.isSelected ? "#000" : "#0F0",
      backgroundColor: state.isSelected ? "#a0a0a0" : "#212529",
    }),
    singleValue: (defaultStyles) => ({ ...defaultStyles, color: "#fff" }),
  }

  return (
    <Select
      options={options}
      styles={customStyles}
      value={selectOption} onChange={handleChange}
    />
  )
}

export default MySelect;


