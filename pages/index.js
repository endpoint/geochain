import { useState, useRef, useEffect } from 'react'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Image from 'next/image'
import ReactMarkdown from 'react-markdown'
import CircularProgress from '@mui/material/CircularProgress';
// import dynamic from 'next/dynamic';
import MySelect from './_selectFeature';

export default function Home() {

  const [userInput, setUserInput] = useState("");
  const [loading, setLoading] = useState(false);
  const [conversationHistory, setConversationHistory] = useState([])
  // TODO: Update initial message
  const [messages, setMessages] = useState([
    { role: "assistant", content: "Hi there! Please select a park you would like to learn about above." }
  ]);
  const [selectedOption, setSelectedOption] = useState(null);

  const messageListRef = useRef(null);
  const textAreaRef = useRef(null);

  // Auto scroll chat to bottom
  useEffect(() => {
    const messageList = messageListRef.current;
    messageList.scrollTop = messageList.scrollHeight;
  }, [messages]);

  // Focus on text field on load
  useEffect(() => {
    textAreaRef.current.focus();
  }, []);

  // Handle errors
  const handleError = () => {
    setMessages((prevMessages) => [...prevMessages, { role: "assistant", content: "To get started, fork this Repl and add the environment variable `OPENAI_API_KEY` as a Secret. Make an account on [OpenAI](https://platform.openai.com/docs/api-reference) to get an API key." }]);
    setLoading(false);
    setUserInput("");
  }

  // Handle form submission
  const handleSubmit = async (e) => {
    e.preventDefault();

    if (userInput.trim() === "") {
      return;
    }

    setLoading(true);
    const context = [...messages, { role: "user", content: userInput }];

    setMessages(context);
    const formattedConversationHistory = context.map(c => `${c.role}: ${c.content}`)

    // Send chat history to API
    const response = await fetch("/api/llm-chat", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ conversationHistory: formattedConversationHistory, prompt: userInput }),
    });

    if (!response.ok) {
      handleError();
      return;
    }

    // Reset user input
    setUserInput("");

    const data = await response.json();
    console.log("DATA", data)
    setMessages((prevMessages) => [...prevMessages, { role: "assistant", content: data.result }]);
    setLoading(false);

  };

  // Prevent blank submissions and allow for multiline input
  const handleEnter = (e) => {
    if (e.key === "Enter" && userInput) {
      if (!e.shiftKey && userInput) {
        handleSubmit(e);
      }
    } else if (e.key === "Enter") {
      e.preventDefault();
    }
  };



  function selectOption(index) {
    //call this to triger a selection
    return index
  }

  async function onFeatureSelect(index, label) {
    //this gets called on eatch selection 
    console.log(index)

    setLoading(true);
    const context = [...messages, { role: "user", content: userInput }];

    setMessages(context);

    const formattedConversationHistory = context.map(c => `${c.role}: ${c.content}`)
    // let name = feature.properties.NAME_E;

    //todo send mesage with name
    let contents = "What can you tell me about this place: " + label;
    // alert(feature.properties.NAME_E)
    const response = await fetch("/api/llm-chat", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ prompt: contents }),
    });



    if (!response.ok) {
      // handleError();
      return;
    }

    // Reset user input
    setUserInput("");

    const data = await response.json();
    console.log("DATA", data)
    setMessages((prevMessages) => [...prevMessages, { role: "assistant", content: data.result }]);
    setLoading(false);



    //   console.log(response)
    //   setMessages((prevMessages) => [...prevMessages, { role: "assistant", content: response.result }]);
  }


  return (
    <>
      <Head>
        <title>GeoChain Demo</title>
        <meta name="description" content="GPT-4 interface" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.topnav}>
        <div className={styles.navlogo}>
          <a href="/">GPT-4 Chat UI</a>
        </div>
        <div className={styles.navlinks}>
          <a href="https://platform.openai.com/docs/models/gpt-4" target="_blank">Docs</a>
          <a href="https://replit.com/@zahid/GPT-4-UI" target="_blank">Replit100</a>
        </div>
      </div>
      <MySelect onFeatureSelect={onFeatureSelect} selectOption={selectOption} />
      <main className={styles.main}>

        <div className={styles.cloud}>
          <div ref={messageListRef} className={styles.messagelist}>
            {messages.map((message, index) => {
              return (
                // The latest message sent by the user will be animated while waiting for a response
                <div key={index} className={message.role === "user" && loading && index === messages.length - 1 ? styles.usermessagewaiting : message.role === "assistant" ? styles.apimessage : styles.usermessage}>
                  {/* Display the correct icon depending on the message type */}
                  {message.role === "assistant" ? <Image src="/openai.png" alt="AI" width="30" height="30" className={styles.boticon} priority={true} /> : <Image src="/usericon.png" alt="Me" width="30" height="30" className={styles.usericon} priority={true} />}
                  <div className={styles.markdownanswer}>
                    {/* Messages are being rendered in Markdown format */}
                    <ReactMarkdown linkTarget={"_blank"}>{message.content}</ReactMarkdown>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
        <div className={styles.center}>

          <div className={styles.cloudform}>
            <form onSubmit={handleSubmit}>
              <textarea
                disabled={loading}
                onKeyDown={handleEnter}
                ref={textAreaRef}
                autoFocus={false}
                rows={1}
                maxLength={512}
                type="text"
                id="userInput"
                name="userInput"
                placeholder={loading ? "Waiting for response..." : "Type your question..."}
                value={userInput}
                onChange={e => setUserInput(e.target.value)}
                className={styles.textarea}
              />
              <button
                type="submit"
                disabled={loading}
                className={styles.generatebutton}
              >
                {loading ? <div className={styles.loadingwheel}><CircularProgress color="inherit" size={20} /> </div> :
                  // Send icon SVG in input field
                  <svg viewBox='0 0 20 20' className={styles.svgicon} xmlns='http://www.w3.org/2000/svg'>
                    <path d='M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z'></path>
                  </svg>}
              </button>
            </form>
          </div>
          <div className={styles.footer}>
            <p>Powered by <a href="https://openai.com/" target="_blank">OpenAI</a>. Built on <a href="https://replit.com/@zahid/GPT-4-UI" target="_blank">Replit</a>.</p>
          </div>
        </div>
      </main>
    </>
  )
}
