import { MapContainer, Marker, Popup, TileLayer, GeoJSON, } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import React, { Component } from 'react';
import sampleData from "../raw-data/sample_data.json"


function listData() {
  let labels = sampleData.features.forEach(f => {
    return f.properties.NAME_E
  })
}
// get the geoJson containing the heatmap
function getGeoJsonData() {
  ChatCompletionRequestMessageRoleEnum.log()
  return sampleData;
}

function styleData(feature) {
  return {
    // the fillColor is adapted from a property which can be changed by the user (segment)
    fillColor: "green",
    weight: 0.3,
    //stroke-width: to have a constant width on the screen need to adapt with scale 
    opacity: 1,
    color: "hotpink",
    dashArray: '3',
    fillOpacity: 0.5
  };
};



const Map = () => {
  return (
    <MapContainer center={[48.158757304569235, -85.330810546875]} zoom={11} scrollWheelZoom={false} style={{ height: 400, width: "100%" }}>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[48.158757304569235, -85.330810546875]}>
        <Popup>
          A pretty CSS3 popup. {sampleData.features[0].properties.NAME_E} <br /> Easily customizable.
        </Popup>
      </Marker>


    </MapContainer>
  )
}

export default Map
