import generateResponse from "../../lib/generateResponse.js";
// import promptSync from 'prompt-sync';
import { ChatOpenAI } from "langchain/chat_models/openai";
import { HNSWLib } from "langchain/vectorstores/hnswlib";
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import { VectorDBQAChain } from "langchain/chains";
import { SerpAPI, ChainTool } from "langchain/tools";
import { Calculator } from "langchain/tools/calculator";
import { initializeAgentExecutor } from "langchain/agents";
import { BufferMemory } from "langchain/memory";

// const prompt = promptSync();

const vectorSaveDir = process.env.VECTOR_STORE_DIR
const openaiApiKey = process.env.OPENAI_API_KEY

export default async function(req, res) {
  const { conversationHistory, prompt } = req.body
  const model = new ChatOpenAI({ temperature: 0, openAIApiKey: openaiApiKey  });
  const store = await HNSWLib.load(vectorSaveDir, new OpenAIEmbeddings({
    openAIApiKey: process.env.OPENAI_API_KEY
  }))

  const chain = VectorDBQAChain.fromLLM(model, store);

  const qaTool = new ChainTool({
    name: "provincial-park-tool",
    description:
      "Provincial Park Tool - useful for when you need to ask questions about Canadian Provincial Parks.",
    chain: chain,

  });

  const tools = [
    qaTool, new Calculator(),
  ];

  const executor = await initializeAgentExecutor(
    tools,
    model,
    "chat-conversational-react-description",
    true
  );
  executor.memory = new BufferMemory({
    returnMessages: true,
    memoryKey: "chat_history",
    inputKey: "input",
  });
  console.log("Loaded agent.");

  const input0 = "What is the context object look like? print the contents";

  const result0 = await executor.call({ input: input0 });

  console.log('DATA==>', result0);
  console.log('POSTED')



  const answer = await generateResponse({
    prompt,
    history: conversationHistory
  });
  console.log("ANSWER", answer)

  res.status(200).json({ result: answer.text })
}