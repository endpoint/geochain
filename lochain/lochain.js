import {HNSWLib} from "langchain/vectorstores/hnswlib";

class GPT {
    constructor(config) {
    }
    chat(){}

    getEmbedings() {}
}

/**
 * export declare class HNSWLib extends SaveableVectorStore {
 *     _index?: HierarchicalNSWT;
 *     docstore: InMemoryDocstore;
 *     args: HNSWLibBase;
 *     constructor(embeddings: Embeddings, args: HNSWLibArgs);
 *     addDocuments(documents: Document[]): Promise<void>;
 *     private static getHierarchicalNSW;
 *     private initIndex;
 *     get index(): HierarchicalNSWT;
 *     private set index(value);
 *     addVectors(vectors: number[][], documents: Document[]): Promise<void>;
 *     similaritySearchVectorWithScore(query: number[], k: number): Promise<[Document<Record<string, any>>, number][]>;
 *     save(directory: string): Promise<void>;
 *     static load(directory: string, embeddings: Embeddings): Promise<HNSWLib>;
 *     static fromTexts(texts: string[], metadatas: object[] | object, embeddings: Embeddings, dbConfig?: {
 *         docstore?: InMemoryDocstore;
 *     }): Promise<HNSWLib>;
 *     static fromDocuments(docs: Document[], embeddings: Embeddings, dbConfig?: {
 *         docstore?: InMemoryDocstore;
 *     }): Promise<HNSWLib>;
 *     static imports(): Promise<{
 *         HierarchicalNSW: typeof HierarchicalNSWT;
 *     }>;
 * }
 */
class VectorStore {
    constructor(gpt, docs, options) {
        this.docstore = new InMemoryDocstore();
        this.args = new HNSWLibArgs(config); // assuming config contains the necessary arguments
        this.embedings = gpt.getEmbedings();
        this.vectorStore = new HNSWLib(this.embedings, {
            docstore: this.docstore,
            index: new HierarchicalNSW()
        })
    }
    save(){}
    load(){}
    insert(){}
    search(){}
    lookup(){}
    delete(){}
}


class Lochain {

    constructor(config) {
        let defaults = {
            openaiApiKey: null,
            saveDir: "vector-store-save",
            model: "gpt-3.5-turbo",

            docs: [], //docs to insert into vector store
            contextLimitTokens: 2048,
            contextLimitChunks: 10,
            summary: true, // sumarize each chunk along side embeding
            qaGen: true, // create qa embeddings for generated questions
        }

        this.config = Object.assign(config, defaults);

    }

    static create(config){}
    static load(vectorStoreDir){
        //load vector store + config
        // initialize empty
        // set vector store
    }
    save(vectorStoreDir){
        //save vector stroke
        //save config
    }
    insert(docs, summarize=false, qaGen=false){
        //add docs to vector store
    }
    search(search){
        //lookup related vectors
    }
    lookup(id){
        // lookup single vector
    }
    chat(msgs){
        //submit chat
    }
    query(search, id){
        //lookup both and cha
    }


}
export default lochain;