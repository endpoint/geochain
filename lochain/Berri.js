/**
 * NOTES:
 * Templates - are configurations of berry settings that can have an instance created off them
 *      create(app_config)
 *
 * Instances - are templates loded with data so theycan catual server responses
 *      create(termplate,data)
 *      query(text): Query
 *
 * Query - are submited to instances and provide a way of accessing the information
 *      modify
 *      inspect
 *      show
 *      ...
 *
 * GPT - a large langare general odel
 *      chat(msgs) -> nextMsg
 *
 * VectorStore - a backend to stor vecotr data
 *      new(chunks)
 *      query(search)
 * LOC - Lochain - Location Observation Chain
 *      setup(app_config)
 *          _initGPT
 *          _initVectorStore
 *
 *      insert(chunk, options) adds a single chunk or array of chunks to vector db
 *      query(search, options)
 *         _search(search, limit, options) -> searchResults from vector stor array of vecotrs
 *         _buildMsgs(search, searchResults, options})
 *         _chat(msgs, options)
 *
 *      load(fs_path) -> save to disk
 *      save(fs_path) -> load from disk
 *
 */


class Template {
    constructor(berri_config) {
        this.app_config = berri_config;
        this.isInit = false;

        this.init();
    }

    async init() {
        //todo submit request to api.berri
        this.isInit = true;
    }
}
class Instance {

}

class Berri {

    constructor(api_endpoint, key) {

        this.endpoint =  api_endpoint ||"https://api.berri.ai"

        let docUrl = "https://enthusiastic-gold-impala.mintlify.app/api-reference/endpoint/"

        this._endpoints = [
            "/create_template",//post
            "/create_app",//post
            "/query", //get
            "/update_instance_data",//post query:user_email* instance_id*; body: data The text data you want to add to your instance Example
            "/finetune_instance", //post  body: data[{context, correct_response}]
            "/delete_instance"// post
        ]
    }




    /**
     * Creates a new file configuration template with customizable options.
     * @param {Object} [app_config={}] - The configuration options for the template.
     * @param {Object} [app_config.advanced] - Advanced options for the template.
     * @param {string} [app_config.advanced.intent="qa_gen"] -
     * @param {string} [app_config.advanced.search="default"] - For each piece of information (chunk), we can generate
     *      either 5 potential questions that a user might ask about it (qa_gen) or a
     *      summary of the information (summarize) to make it easier for the user to understand. This is especially
     *      useful when dealing with technical or complex information that the user may not be familiar with.
     * @param {string} [app_config.app_type="simple"] - Selects the type of search to use. "simple" finds the most similar chunks to the user's question,
     *      while "complex" breaks down the question into sub-components to find the most relevant chunks for each component.
     * @param {number} [app_config.output_length=512 ] - The desired output length of the model.
     * @param {string} [app_config.prompt] - Instructions for the model on how to answer user questions.
     * @returns {Promise<Object>} - A Promise that resolves to the created template.
     */
    create_template(app_config = {}) {
        const defaultConfig = {
            advanced: {
                intent: "qa_gen",
                search: "default" // summary best
            },
            prompt: "Be legal and ethical",
            output_length: 512,
        };

        const mergedConfig = { ...defaultConfig, ...app_config };

        const endpoint = `${this.endpoint}/create_template`;
        const formdata = new FormData();
        formdata.append("app_config", JSON.stringify(mergedConfig));
        const requestOptions = {
            method: "POST",
            body: formdata,
            redirect: "follow",
        };

        return fetch(endpoint, requestOptions).then((response) => response.json());
    }


    create_instance() {

    }

    query(search) {

    }

}
Berri.Template = Template;
Berri.Instance = Instance;
// Berri.Query = Query;
export default Berri