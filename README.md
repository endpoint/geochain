## Cleaning Data
  First we download from gov website
  then we proses it into 


  let data = {
    layer_name: {
      type: "FeatureColection"
      features: [
        {
          type: "Polygon",
          properties: {},
          geometry: "..."
        }
      }
    ]
  }

  layerToDocs
  docsToSummary -> adds summary
  docsToSearch
  searchToWiki
  docToSummary
  
  layerToSumary
  


  


  OUTPUT: RAW JSON + TEXT BLOB like pdfs

## Training Layer #1
- Summary prompt has context to data shape and definitions

  Is this creating the vector embeding from the json
  FI

  Read in an array of layers

  for each layer there are many features

  for each feature create a new chunk and creat a sumary of that chunk

  save that in the vecotr store for later

 
  ... later a question is ascted and can look up the sumary. based on a question + a feature

  Output: Vector store
  output: geojson_display_index.json {
    type: "FeatureColection",
    features: [
      {
        type: "Polygon"
        properties: {
          id: "string",
          label: "string"
        },
        geomitry: ...
      },
      ...
    ]
  }
  
  
## LLM Payload
- Include filtered raw data as context for similarity search
- Location name
- Owner

## LLM Completion
- Summary

## TODO:
- Additional data per park using similaritySearch with trained Vector store? Berri bot?

## Related resources: 
- LLM replit: https://replit.com/join/ceuepgyvcn-theoweller
- GeoJSON replit: https://replit.com/@ep42/testnodetemplate#data_delisted.json
- Original Data: https://data-donnees.ec.gc.ca/data/species/protectrestore/canadian-protected-conserved-areas-database/ProtectedConservedAreaUserManual.pdf
---






API: 
renderMap
getData

generateResponse(feature, mesages)
generateEmbeding(feature)
useEmbeding(embeding, mesages)





Page load -> api_req_data -> UI polygon clicked -> api_introspect_data(input: feature) -> chat box rendered -> Any question / chat can be initiated based on trained info -> user interacts with chat loop

inputs(feature) {
  look up all vectors simalerto the features: 

  //gather all documents
  
  //generate sumaryies
  
  //generate embeding

  
  return {feature}
}





## GPT-4 Chatbot UI

Build your own GPT-4 chatbot frontend with this open source Next.js template! Simply add `OPENAI_API_KEY` as a Secret in this Repl.

Don't have early access to GPT-4? Replace `gpt-4` with `gpt-3.5-turbo` in `pages/api/chat.js`.

**Note: due to insanely high traffic, I have downgraded this to GPT 3.5 until further notice.**

## Getting Started

Hit the run button to start the development server.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on `/api/chat`. This endpoint can be edited in `pages/api/chat.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

## Productionizing your Next App

To make your next App run smoothly in production make sure to [turn this Repl to an Always On Repl.](https://docs.replit.com/hosting/enabling-always-on)

You can also produce a production build by running `npm run build` and [changing the run command](https://docs.replit.com/programming-ide/configuring-repl#run) to `npm run start`.