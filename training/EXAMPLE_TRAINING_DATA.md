{"type":"Feature","properties":{"OBJECTID":5,"PARENT_ID":"240001948","NAME_E":"Marais du Nord Natural Area Under Private Stewardship","NAME_F":"Milieu naturel de conservation volontaire Marais du Nord","NAME_IND":null,"ZONE_ID":"240001948","ZONEDESC_E":null,"BIOME":"T","JUR_ID":"Private","PA_OECM_DF":"5","IUCN_CAT":"9","IPCA":null,"IND_TERM":null,"O_AREA_HA":40,"LOC":11,"MECH_E":"Companies Act, Civil code of Quebec, Canada Corporations Act","TYPE_E":"Natural Area Under Private Stewardship","OWNER_TYPE":"6","OWNER_E":"Private","GOV_TYPE":"7","MGMT_E":"Private","STATUS":"0","ESTYEAR":null,"QUALYEAR":"1900","DELISTYEAR":"2021","SUBSRIGHT":null,"CONS_OBJ":"0","NO_TAKE":"0","NO_TAKE_HA":0,"MPLAN":null,"MPLAN_REF":"http://www.environnement.gouv.qc.ca/biodiversite/prive/terres-priv.htm","M_EFF":null,"AUDITYEAR":null,"AUDITRES":null,"PROVIDER":"24","SHAPE_Length":2387.660154664898,"SHAPE_Area":356255.40659939754},"geometry":{"type":"MultiPolygon","coordinates":[[[[-71.3882176,46.9634686],[-71.3805163,46.9634696],[-71.3805174,46.9689034],[-71.3882195,46.9689695],[-71.3882176,46.9634686]]]]}}

---

## Summary of Marais du Nord Natural Area Under Private Stewardship
- Name: Marais du Nord Natural Area Under Private Stewardship
- Established Year: 1900
- Type of Park or Protected Area: Natural Area Under Private Stewardship
- Biome: Temperate
- Management Mechanism: Companies Act, Civil code of Quebec, Canada Corporations Act
- Entity responsible for managing the park: Private
- Owner: Private
- Type of Ownership: Private
- Government Type: Not applicable
- Area: 40 hectares
- Status: Delisted in 2021
- Conservation Objective: No conservation objective specified
- Zone ID: 240001948
- Shape Length: 2387.660154664898
- Shape Area: 356255.40659939754