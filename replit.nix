{ pkgs }: {
	deps = [
  pkgs.ncdu
  pkgs.python310Full
	pkgs.unzip
  pkgs.wget
  pkgs.nano
  pkgs.sudo
  pkgs.sudo
  pkgs.nodejs-18_x
    pkgs.nodePackages.typescript-language-server
    pkgs.yarn
    pkgs.replitPackages.jest
	];
}