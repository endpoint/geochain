// const { OpenAI } = require('langchain/llms');
// const { LLMChain, PromptTemplate } = require('langchain');
// const { HNSWLib } = require('langchain/vectorstores');
// const { OpenAIEmbeddings } = require('langchain/embeddings');
// const summaryPrompt = require('./summaryPrompt.js');

import { OpenAI } from 'langchain/llms';
import { LLMChain, PromptTemplate } from 'langchain';
import { HNSWLib } from "langchain/vectorstores";
import { OpenAIEmbeddings } from 'langchain/embeddings';

const summaryPrompt = `The below data is an entry from The Canadian Protected and Conserved Areas Database (CPCAD).

Here's an explanation of the properties included the entry:

- BIOME: The type of biome that the park is located in.
- GOV_TYPE: The type of government agency that manages the park.
- ESTYEAR: The year in which the park was established.
- NAME_E: The name of the park in English.
- JUR_ID: The unique identifier for the park's jurisdiction.
- LOC: The location of the park in latitude and longitude.
- MECH_E: The management mechanism used to protect the park.
- MGMT_E: The entity responsible for managing the park.
- OBJECTID: The unique identifier for the park.
- OWNER_E: The name of the park's owner.
- OWNER_TYPE: The type of entity that owns the park.
- TYPE_E: The type of park or protected area.
- ZONEDESC_E: A description of the park's zoning.
- ZONE_ID: The unique identifier for the park's zone.

Using the explanation of each property create a formatted summary in markdown format using the data from this entry.

INSTRUCTIONS:
- Make this summary concise and human-readable

EXAMPLE SUMMARY:
## Summary of Mill River Provincial Park
- Name: Mill River Provincial Park
- Established Year: 1968
- Type of Park or Protected Area: Provincial Park
- Biome: Temperate
- Management Mechanism: PEI Recreation Development Act
- Entity responsible for managing the park: PEI Department Of Tourism
- Owner: PEI Department of Tourism
- Type of Ownership: Government
- Government Type: Provincial
- Area: 153 hectares
- Status: Delisted in 2017
- Conservation Objective: No conservation objective specified
- Zone ID: 113005000
- Shape Length: 5988.8678707300605
- Shape Area: 1166609.0753774035

ENTRY:
{rawData}
`


// OpenAI Configuration
const model = new OpenAI({
  temperature: 0,
  openAIApiKey: process.env.OPENAI_API_KEY,
  modelName: 'gpt-3.5-turbo'
});

// Parse and initialize the Prompt
const prompt = new PromptTemplate({
  template: summaryPrompt,
  inputVariables: ["rawData"]
});

// Create the LLM Chain
const llmChain = new LLMChain({
  llm: model,
  prompt
});

/** 
 * Generates a Response based on history and a prompt.
 * @param {string} history - 
 * @param {string} prompt - Th
 */
const generateSummary = async (filteredProperties) => {
  // Load the Vector Store from the `vectorStore` directory
  let store = await HNSWLib.load("vectorStore", new OpenAIEmbeddings({
      openAIApiKey: process.env.OPENAI_API_KEY
    }))
  
  console.clear();

  return await llmChain.predict({
    prompt,
    rawData: transformAttributesToString(filteredProperties)
  })
}

function transformAttributesToString(obj) {
  const props = obj;
  let output = '';

  for (const [key, value] of Object.entries(props)) {
    output += `${key}: ${value}\n`;
  }

  return output;
}

export default generateSummary;