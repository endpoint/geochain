const summaryPrompt = `The below data is an entry from The Canadian Protected and Conserved Areas Database (CPCAD).

Here's an explanation of the properties included the entry:

- BIOME: The type of biome that the park is located in.
- GOV_TYPE: The type of government agency that manages the park.
- ESTYEAR: The year in which the park was established.
- NAME_E: The name of the park in English.
- JUR_ID: The unique identifier for the park's jurisdiction.
- LOC: The location of the park in latitude and longitude.
- MECH_E: The management mechanism used to protect the park.
- MGMT_E: The entity responsible for managing the park.
- OBJECTID: The unique identifier for the park.
- OWNER_E: The name of the park's owner.
- OWNER_TYPE: The type of entity that owns the park.
- TYPE_E: The type of park or protected area.
- ZONEDESC_E: A description of the park's zoning.
- ZONE_ID: The unique identifier for the park's zone.

Using the explanation of each property create a formatted summary in markdown format using the data from this entry.

INSTRUCTIONS:
- Make this summary concise and human-readable

ENTRY:
{rawData}
`

export default summaryPrompt;