import { OpenAI } from 'langchain/llms/openai';
import { PromptTemplate } from "langchain/prompts";
import { LLMChain } from "langchain/chains";
import { HNSWLib } from "langchain/vectorstores/hnswlib";
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import promptTemplate from './basePrompt.js';
import chatPromptTemplate from './chatPrompt.js';


import { ChatOpenAI } from "langchain/chat_models/openai";

// OpenAI Configuration
const model = new OpenAI({
  temperature: 0,
  openAIApiKey: process.env.OPENAI_API_KEY,
  modelName: 'gpt-3.5-turbo'
});

const chat = new ChatOpenAI({ temperature: 0, openAIApiKey: process.env.OPENAI_API_KEY });

// Parse and initialize the Prompt
const prompt = new PromptTemplate({
  template: promptTemplate,
  inputVariables: ["history", "context", "prompt"]
});

const chatPrompt = chatPromptTemplate

const chatChain = new LLMChain({
  prompt: chatPrompt,
  llm: chat,
});


// Create the LLM Chain
const llmChain = new LLMChain({
  llm: model,
  prompt
});

/** 
 * Generates a Response based on history and a prompt.
 * @param {string} history - 
 * @param {string} prompt - Th
 */
const generateResponse = async ({
  history,
  prompt
}) => {
  // Load the Vector Store from the `vectorStore` directory
  let store = await HNSWLib.load("vectorStore", new OpenAIEmbeddings({
    openAIApiKey: process.env.OPENAI_API_KEY
  }))
  console.clear();



  // Search for related context/documents in the vectorStore directory
  const data = await store.similaritySearch(prompt, 1);
  const context = [];
  data.forEach((item, i) => {
    context.push(`Context:\n${item.pageContent}`)
  });

  const responseB = await chatChain.call({
    prompt,
    context: context.join('\n\n'),
    history

  });



  return await chatChain.call({
    prompt,
    context: context.join('\n\n'),
    history

  });
}

export default generateResponse;